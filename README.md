# 19S-ITT2-Dashboard-example

This project contains an example of a dashboard used to communicate with the api at: https://github.com/moozer/restex.git

## Used 3rd party libs 

    * JQuery https://jquery.com/ 
    * Bootstrap-material-design https://fezvrasta.github.io/bootstrap-material-design/
    * chart.js https://www.chartjs.org/
    * Fetch API (included in JQuery) https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API
